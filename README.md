# Komputer Store

## Assignment 1 - Front-end Development

Dynamic webpage built with JavaScript, HTML and CSS. Bootstrap was also used. 

## Functionalities
#### Bank
The bank shows your bank balance and amount of loan. You can also get a new loan if you don't have one. 
#### Work
The work button can give you NOK100 if you click it. If you have outstanding loan, 10% of your salary is deducted and transferred to the loan amount, if you transfer the money. You can also repay your outstanding loan.
#### Laptop
The laptop selection shows a list of available computers. Information about a selected laptop is shown when changing a laptop.

## Contributor
Anne Jacobsen Rike
