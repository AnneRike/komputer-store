// DOM Elements
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const getLoanButton = document.getElementById("getLoanButton");  // Button
const totalBalanceElement = document.getElementById("totalBalance");

const laptopsElement = document.getElementById("laptops");
const specsElement = document.getElementById("specs");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const imageElement = document.getElementById("image");
const stockElement = document.getElementById("stock");
const buyNowButton = document.getElementById("buyButton");  // Button

const showBalanceElement = document.getElementById("salary");
const workButton = document.getElementById("workButton");  // Button
const transferButton = document.getElementById("transferButton");  // Button
const repayLoanButton = document.getElementById("repayLoanButton");  // Button

// Variables 
let payBalance = 0;
let bankBalance = 0;
let loan = 0;
let laptops = [];
let selectedLaptopPrice = 0;


// Fetch computers from API
fetch("https://hickory-quilled-actress.glitch.me/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToSelect(laptops))
  .catch((error) => console.error(error));

// Iterates through laptops to add them to the selection-menu 
const addLaptopsToSelect = (laptops) => {
    laptops.forEach(laptop => addLaptopToSelection(laptop));
}

// Add laptops to laptop-selection
const addLaptopToSelection = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.text = laptop.title;
    laptopsElement.appendChild(laptopElement);
    getInformation(laptops[0])
}

// Refresh info to newly selected laptop
function handleLaptopInformationChange(e) {
    const selectedIndex = e.target.selectedIndex;
    const selectedLaptop = laptops[selectedIndex];  // e.target.selectedIndex
    getInformation(selectedLaptop)
}

// Display information about current selected laptop
function getInformation (laptop) {
    specsElement.innerText = laptop.specs;
    titleElement.innerText = laptop.title;
    descriptionElement.innerText =laptop.description;
    priceElement.innerText = 'Price: ' + 'NOK ' + laptop.price;
    stockElement.innerText = 'Stock: ' + laptop.stock;
    imageElement.setAttribute("src", "https://hickory-quilled-actress.glitch.me/" + laptop.image )
    selectedLaptopPrice = laptop.price;
}


//set bank balance
function setInformationBank(){
    balanceElement.innerText = 'Balance: ' + 'NOK ' + bankBalance;
}

//set loan balance
function setInformationLoan(){
    loanElement.innerText = 'Loan: ' + ' NOK ' + loan;
}

//set pay balance
function setInformationPayBalance(){
    showBalanceElement.innerText = 'Pay Balance: ' + ' NOK ' + payBalance;
}

// Initialize:
setInformationBank()
setInformationLoan()
setInformationPayBalance()

// Get loan function 
function getLoan() {
    if (loan > 0) { // Check if you have an existing loan
        alert("You can not get another loan before you pay of your previous loan");
        return true;
    }
    else {
        while (true) {
            let loanAmount = prompt("Enter loan amount: ");
            if (loanAmount > 2 * bankBalance) {
                alert("You can not loan more than double your current bank balance");
            }
            else if (isNaN(loanAmount) || loanAmount < 0) {
                alert("Enter a positive number less than double your current bank balance or 0 to exit");
            }
            else if (loanAmount) {
                alert("Loan submitted");
                loanAmount = parseInt(loanAmount);
                loan = loanAmount;
                bankBalance += loanAmount;
                setInformationBank();
                setInformationLoan();
                repayLoanButton.hidden = false;
                return true;
            }
            else {
                return true; // To be able to click "cancel"
            }
        }
    }
}

// Add NOK 100 every time you press work-button
function work() {
    payBalance += 100;
    setInformationPayBalance();
}

// Transfer money to the bank
function transferMoney() {
    if (loan == 0) {
        bankBalance += payBalance;
    }
    else {
        loan -= payBalance*0.1;
        overflowAmount = 0;
        if (loan <= 0) {
            overflowAmount = -loan;
            loan = 0;
            repayLoanButton.hidden = true;
        }
        bankBalance += (payBalance*0.9 + overflowAmount);
    }
    payBalance = 0; 
    setInformationPayBalance();
    setInformationBank();
    setInformationLoan();
}

// Repay loan
function repayLoan() {
    loan -= payBalance;
    payBalance = 0;
    if (loan <= 0) {
        repayLoanButton.hidden = true;
        payBalance -= loan
        loan = 0
    }
    setInformationLoan();
    setInformationPayBalance()
}

// Buy a laptop
function buyLaptop() {
    if (bankBalance >= selectedLaptopPrice) {
        bankBalance -= selectedLaptopPrice;
        setInformationBank();
        alert("You bough a new laptop!")
    }
    else {
        alert("You do not have enough money in the bank balance to buy this computer");
    }
}

// Event handlers
getLoanButton.addEventListener("click", getLoan);
workButton.addEventListener("click", work);
transferButton.addEventListener("click", transferMoney);
repayLoanButton.addEventListener("click", repayLoan);
buyNowButton.addEventListener("click", buyLaptop);
laptopsElement.addEventListener("change", handleLaptopInformationChange);